package Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.nikita.handtrainer.R;

import java.util.ArrayList;
import java.util.List;

import Objects.StatisticsRecord;

/**
 * Created by Nikita on 22.03.2018.
 */

public class StatisticsRecordAdapter extends BaseAdapter{

    Context context;
    ArrayList<StatisticsRecord> records;
    LayoutInflater layout;

    public StatisticsRecordAdapter(Context context, ArrayList<StatisticsRecord> records) {
        this.context = context;
        this.records = records;
        this.layout = (LayoutInflater)this.context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return this.records.size();
    }

    @Override
    public Object getItem(int position) {
        return this.records.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        if (view == null) {
            view = this.layout.inflate(R.layout.record_item, parent, false);
        }

        StatisticsRecord record = getRecord(position);

        ((TextView)view.findViewById(R.id.idView)).setText(String.valueOf(record.getId()));
        ((TextView)view.findViewById(R.id.dateView)).setText(record.getDate());
        ((TextView)view.findViewById(R.id.scoreView)).setText(String.valueOf("Количество: " + record.getScore()));
        ((TextView)view.findViewById(R.id.timeView)).setText(
                "Время: " + String.format("%02d:%02d", record.getTime() / 60, record.getTime() % 60));

        return view;
    }

    StatisticsRecord getRecord(int position) {
        return ((StatisticsRecord)getItem(position));
    }
}
