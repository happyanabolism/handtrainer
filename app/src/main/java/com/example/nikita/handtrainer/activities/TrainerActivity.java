package com.example.nikita.handtrainer.activities;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.PersistableBundle;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.nikita.handtrainer.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import DataBases.StatisticsDB;
import Objects.StatisticsRecord;

public class TrainerActivity extends AppCompatActivity implements SensorEventListener {

    private Button startTrainingButton;
    private Button stopTrainingButton;
    private Button statisticsTrainingButton;
    private Chronometer chronometer;
    private ImageView arrowView;
    private TextView accelerometerView;
    private TextView scoreView;

    private boolean runningTrainer = false;
    private long trainingTime;
    private int score = 0;
    private boolean left = false;
    private boolean right = false;

    private final int MAX_SCORE = 100;

    private SensorManager sensorManager;
    private Sensor accelerometer;
    private long lastUpdateAccelerometer = 0;
    private int accelerometerUpdateFrequency = 80;

    StatisticsDB statisticsDBHelper;
    SQLiteDatabase statisticsDB;


    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trainer);

        statisticsDBHelper = new StatisticsDB(this);
        statisticsDB = statisticsDBHelper.getWritableDatabase();

        accelerometerView = (TextView)findViewById(R.id.accelerometr);
        scoreView = (TextView)findViewById(R.id.progressTraining);
        chronometer = (Chronometer)findViewById(R.id.timeTraining);
        arrowView = (ImageView)findViewById(R.id.arrowImage);

        stopTrainingButton = (Button)findViewById(R.id.stopTraining);
        startTrainingButton = (Button)findViewById(R.id.startTraining);
        startTrainingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        startTraining();
                        stopTrainingButton.setVisibility(View.VISIBLE);
                        startTrainingButton.setVisibility(View.GONE);
                    }
                }, 500);
            }
        });

        stopTrainingButton.setVisibility(View.GONE);
        stopTrainingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopTraining();
                stopTrainingButton.setVisibility(View.GONE);
                startTrainingButton.setVisibility(View.VISIBLE);
            }
        });

        statisticsTrainingButton = (Button)findViewById(R.id.statisticsTraining);
        statisticsTrainingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TrainerActivity.this, StatisticsActivity.class);
                startActivity(intent);
            }
        });

        this.updateProgressView();

        sensorManager = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void onBackPressed() {

    }

    private void updateProgressView() {
        scoreView.setText(String.valueOf(score) + " / " + String.valueOf(MAX_SCORE));
        if (score == MAX_SCORE) {
            stopTraining();
            stopTrainingButton.setVisibility(View.GONE);
            startTrainingButton.setVisibility(View.VISIBLE);
        }
    }

    private void updateArrowView(float x) {
        if (x > 90)
            arrowView.setRotation(90);
        else if (x < -90)
            arrowView.setRotation(-90);
        else
            arrowView.setRotation(x);
    }

    private boolean checkRotation(float x) {
        if (x >= 90 && left == false) {
            left = true;
            return false;
        }
        else if (x <= -90 && left == true){
            left = false;
            right = !left;
            return true;
        }

        return false;
    }

    private void startTraining() {
        runningTrainer = true;
        score = 0;
        statisticsTrainingButton.setEnabled(false);
        this.updateProgressView();
        startTimer();
    }

    private void stopTraining() {
        runningTrainer = false;
        statisticsTrainingButton.setEnabled(true);
        trainingTime = stopTimer() / 1000;

        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss");

        StatisticsRecord record = new StatisticsRecord(dateFormat.format(date), score, trainingTime);
        statisticsDBHelper.insertRecord(statisticsDB, record);

        score = 0;
    }

    private void startTimer() {
        chronometer.setBase(SystemClock.elapsedRealtime());
        chronometer.start();
    }

    private long stopTimer() {
        chronometer.stop();
        return SystemClock.elapsedRealtime() - chronometer.getBase();
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (!runningTrainer)
            return;

        Sensor sensor = event.sensor;
        if (sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            //get x coordinate from accelerometer
            float x = event.values[0]*10;

            long currTime = System.currentTimeMillis();

            if (currTime - lastUpdateAccelerometer > accelerometerUpdateFrequency) {
                lastUpdateAccelerometer = currTime;

                accelerometerView.setText("X: " + String.valueOf(x));
                this.updateArrowView(x);

                if (checkRotation(x)) {
                    score++;
                    updateProgressView();
                }

            }

        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) { }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        outState.putInt("score", score);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        score = savedInstanceState.getInt("score");
        this.updateProgressView();
    }
}
