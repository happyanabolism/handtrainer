package com.example.nikita.handtrainer.activities;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.nikita.handtrainer.R;

public class SplashScreenActivity extends AppCompatActivity {

    //delay in seconds
    final int DELAY = 5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        //run TrainerActivity after delay
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashScreenActivity.this, TrainerActivity.class);
                startActivity(intent);
            }
        }, DELAY * 1000);
    }
}
