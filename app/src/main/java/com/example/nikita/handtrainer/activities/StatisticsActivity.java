package com.example.nikita.handtrainer.activities;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import com.example.nikita.handtrainer.R;

import java.util.ArrayList;
import java.util.List;

import Adapters.StatisticsRecordAdapter;
import DataBases.StatisticsDB;
import Objects.StatisticsRecord;

public class StatisticsActivity extends AppCompatActivity {

    private Button backToTrainingButton;
    private Button pullToLoadButton;
    private ListView statisticRecordsListView;

    private StatisticsDB statisticsDBHelper;
    private SQLiteDatabase statisticsDB;

    private ArrayList<StatisticsRecord> records;
    private ArrayList<StatisticsRecord> pulledOutRecords;

    private StatisticsRecordAdapter statisticsAdapter;

    final int PART_OF_PULLED_OUT_RECORDS = 50;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics);

        //open database
        statisticsDBHelper = new StatisticsDB(this);
        statisticsDB = statisticsDBHelper.getReadableDatabase();

        records = statisticsDBHelper.getAllData(statisticsDB);

        pulledOutRecords = new ArrayList<>();
        if (records.size() < PART_OF_PULLED_OUT_RECORDS)
            pulledOutRecords = records;
        else
            pulledOutRecords.addAll(records.subList(0, PART_OF_PULLED_OUT_RECORDS));

        statisticsAdapter = new StatisticsRecordAdapter(this, pulledOutRecords);

        statisticRecordsListView = (ListView)findViewById(R.id.statisticRecords);
        statisticRecordsListView.setAdapter(statisticsAdapter);

        backToTrainingButton = (Button)findViewById(R.id.backToTraining);
        backToTrainingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        pullToLoadButton = (Button)findViewById(R.id.pullToLoadButton);
        if (pulledOutRecords.size() < PART_OF_PULLED_OUT_RECORDS) {
            pullToLoadButton.setVisibility(View.GONE);
        }
        pullToLoadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (pulledOutRecords.size() + PART_OF_PULLED_OUT_RECORDS > records.size()) {
                    pulledOutRecords.addAll(records.subList(pulledOutRecords.size(),
                            records.size()));
                } else {
                    pulledOutRecords.addAll(records.subList(pulledOutRecords.size(),
                            pulledOutRecords.size() + PART_OF_PULLED_OUT_RECORDS));
                }
                if (pulledOutRecords.size() == records.size()) {
                    pullToLoadButton.setVisibility(View.GONE);
                }

                statisticsAdapter.notifyDataSetChanged();
            }
        });

    }
}
