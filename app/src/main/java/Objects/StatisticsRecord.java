package Objects;

import android.util.Log;

import java.sql.Time;
import java.util.SimpleTimeZone;

/**
 * Created by Nikita on 22.03.2018.
 */

public class StatisticsRecord {
    private String date;
    private int score;
    private int id;
    private long time;

    public StatisticsRecord(String date, int score, long time) {
        this.date = date;
        this.score = score;
        this.time = time;
    }

    public StatisticsRecord(int id, String date, int score, long time) {
        this.id = id;
        this.date = date;
        this.score = score;
        this.time = time;
    }

    public String getDate() {
        return this.date;
    }

    public int getScore() {
        return this.score;
    }

    public long getTime() {return this.time; }

    public int getId(){ return this.id; }
}
