package DataBases;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

import Objects.StatisticsRecord;

/**
 * Created by Nikita on 21.03.2018.
 */

public class StatisticsDB extends SQLiteOpenHelper {
    private static final String DB_NAME = "statistics";
    private static final int DB_VERSION = 1;
    private static final String DB_TABLE = "statistics_table";

    public StatisticsDB(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    public ArrayList<StatisticsRecord> getAllData(SQLiteDatabase db) {
        Cursor cursor = db.rawQuery("select * from " + DB_TABLE, null);

        ArrayList<StatisticsRecord> records = new ArrayList<>();
        if (cursor.moveToFirst()) {
            int dateIndex = cursor.getColumnIndex("date");
            int scoreIndex = cursor.getColumnIndex("score");
            int timeIndex = cursor.getColumnIndex("time");
            int idIndex = cursor.getColumnIndex("id");
            do {
                records.add(new StatisticsRecord(cursor.getInt(idIndex), cursor.getString(dateIndex),
                        cursor.getInt(scoreIndex), cursor.getInt(timeIndex)));
            } while (cursor.moveToNext());
        }
        cursor.close();

        return records;
    }

    public void insertRecord(SQLiteDatabase db, StatisticsRecord record) {
        db.execSQL("insert into " + DB_TABLE + " (date, score, time) values (?, ?, ?)",
                new String[] {record.getDate(), String.valueOf(record.getScore()), String.valueOf(record.getTime())});
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        //create table with statistics records
        db.execSQL("create table " + DB_TABLE + " ("
                + "id integer primary key autoincrement,"
                + "date text,"
                + "score integer,"
                + "time integer);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists " + DB_TABLE);
    }
}
